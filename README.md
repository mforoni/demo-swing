# demo-swing

Contains several demo projects showing Swing APIs usage:

* demo-swing
* demo-swingbits
* oracle-swing

It also contains several usage demonstration of the architectural pattern [Model-View-Controller](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller):

* mvc-calculator
* [mvc-counter](http://www.austintek.com/mvc/)
* mvc-form
* [mvc-product](https://codereview.stackexchange.com/questions/172573/simple-mvc-crud-with-jdbc)
* [mvc-table](https://examples.javacodegeeks.com/core-java/java-swing-mvc-example/)
* [mvc-tempconvert](http://csis.pace.edu/~bergin/mvc/mvcgui.html)
