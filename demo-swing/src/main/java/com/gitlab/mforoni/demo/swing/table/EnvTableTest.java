package com.gitlab.mforoni.demo.swing.table;

import java.awt.EventQueue;
import java.awt.GridLayout;
import java.util.Map;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

/**
 * See:
 * 
 * https://stackoverflow.com/questions/9132987/jtable-duplicate-values-in-row/9134371#9134371
 * 
 * https://stackoverflow.com/questions/9132987
 */
public class EnvTableTest extends JPanel {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public EnvTableTest() {
    this.setLayout(new GridLayout());
    this.add(new JScrollPane(new JTable(new EnvDataModel())));
  }

  private static class EnvDataModel extends AbstractTableModel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private final Map<String, String> data = System.getenv();
    private final String[] keys;

    public EnvDataModel() {
      keys = data.keySet().toArray(new String[data.size()]);
    }

    @Override
    public String getColumnName(final int col) {
      if (col == 0) {
        return "Key";
      } else {
        return "Value";
      }
    }

    @Override
    public int getColumnCount() {
      return 2;
    }

    @Override
    public int getRowCount() {
      return data.size();
    }

    @Override
    public Object getValueAt(final int row, final int col) {
      if (col == 0) {
        return keys[row];
      } else {
        return data.get(keys[row]);
      }
    }
  }

  private void display() {
    final JFrame f = new JFrame("EnvTableTest");
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.add(this);
    f.pack();
    f.setLocationRelativeTo(null);
    f.setVisible(true);
  }

  public static void main(final String[] args) {
    EventQueue.invokeLater(new Runnable() {

      @Override
      public void run() {
        new EnvTableTest().display();
      }
    });
  }
}
