package com.gitlab.mforoni.demo.swing.table;

import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

/**
 * See:
 * https://stackoverflow.com/questions/15657734/java-get-row-data-from-popupmenu-actionlistener-event/15658573#15658573
 * 
 * @author ?
 */
public class AllTableExample {

  private JTabbedPane tabbedPane;
  private JTable allTable;
  private final AllTableModel allTableModel;

  public AllTableExample() {
    final List<TableData> data = new ArrayList<TableData>();
    data.add(new TableData("John1", "A", "Maths", "Hellen1"));
    data.add(new TableData("John2", "A", "Maths", "Hellen2"));
    data.add(new TableData("John3", "A", "Maths", "Hellen3"));
    data.add(new TableData("John4", "A", "Maths", "Hellen4"));
    data.add(new TableData("John5", "A", "Maths", "Hellen5"));
    allTableModel = new AllTableModel(data);
  }

  public void createUI() {
    final JFrame frame = new JFrame();
    tabbedPane = new JTabbedPane();
    tabbedPane.add("All", getAllTablePanel());
    frame.add(tabbedPane);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("Table Model Example.");
    frame.pack();
    frame.setVisible(true);
  }

  private JPanel getAllTablePanel() {
    final JPanel panel = new JPanel();
    allTable = new JTable(allTableModel);
    final JScrollPane scroll = new JScrollPane(allTable);
    panel.add(scroll);
    allTable.addMouseListener(new MouseAdapter() {

      @Override
      public void mouseReleased(final MouseEvent e) {
        final int r = allTable.rowAtPoint(e.getPoint());
        if (r >= 0 && r < allTable.getRowCount()) {
          allTable.setRowSelectionInterval(r, r);
        } else {
          allTable.clearSelection();
        }
        final int rowindex = allTable.getSelectedRow();
        if (rowindex < 0) {
          return;
        }
        if (e.getComponent() instanceof JTable) {
          final JDialog dialog = new JDialog();
          final int selectedRow = allTable.getSelectedRow();
          dialog.setTitle("Edit Row: " + selectedRow);
          final TableData data =
              ((AllTableModel) allTable.getModel()).getTableData().get(selectedRow);
          final List<TableData> tempData = new ArrayList<TableData>();
          tempData.add(data);
          final AllTableModel tempModel = new AllTableModel(tempData);
          final JTable table = new JTable(tempModel);
          dialog.add(new JScrollPane(table));
          dialog.setLocationRelativeTo(null);
          dialog.pack();
          dialog.setVisible(true);
        }
      }
    });
    return panel;
  }

  public static void main(final String[] args) {
    final Runnable r = new Runnable() {

      @Override
      public void run() {
        new AllTableExample().createUI();
      }
    };
    EventQueue.invokeLater(r);
  }
}


@SuppressWarnings("serial")
class AllTableModel extends AbstractTableModel {

  List<TableData> tableData = new ArrayList<TableData>();
  Object[] columnNames = {"Name", "Grade", "Subject", "Staff"};

  public AllTableModel(final List<TableData> data) {
    tableData = data;
  }

  public List<TableData> getTableData() {
    return tableData;
  }

  @Override
  public String getColumnName(final int column) {
    return columnNames[column].toString();
  }

  @Override
  public int getColumnCount() {
    return columnNames.length;
  }

  @Override
  public int getRowCount() {
    return tableData.size();
  }

  @Override
  public Object getValueAt(final int rowIndex, final int columnIndex) {
    final TableData data = tableData.get(rowIndex);
    switch (columnIndex) {
      case 0:
        return data.getName();
      case 1:
        return data.getGrade();
      case 2:
        return data.getSubject();
      case 3:
        return data.getStaff();
      default:
        return null;
    }
  }
}


class TableData {

  private String name;
  private String grade;
  private String subject;
  private String staff;

  public TableData(final String name, final String grade, final String subject,
      final String staff) {
    super();
    this.name = name;
    this.grade = grade;
    this.subject = subject;
    this.staff = staff;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getGrade() {
    return grade;
  }

  public void setGrade(final String grade) {
    this.grade = grade;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(final String subject) {
    this.subject = subject;
  }

  public String getStaff() {
    return staff;
  }

  public void setStaff(final String staff) {
    this.staff = staff;
  }
}
