package com.gitlab.mforoni.demo.swing.layout;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * https://www.tutorialspoint.com/swing/swing_grouplayout.htm
 * 
 * @author ?
 */
final class GroupLayoutDemo {

  private JFrame mainFrame;
  private JLabel headerLabel;
  private JLabel statusLabel;
  private JPanel controlPanel;
  // private JLabel msglabel;

  private GroupLayoutDemo() {
    prepareGUI();
  }

  public static void main(final String[] args) {
    final GroupLayoutDemo swingLayoutDemo = new GroupLayoutDemo();
    swingLayoutDemo.showGroupLayoutDemo();
  }

  private void prepareGUI() {
    mainFrame = new JFrame("Java SWING Examples");
    mainFrame.setSize(400, 400);
    mainFrame.setLayout(new GridLayout(3, 1));
    headerLabel = new JLabel("", SwingConstants.CENTER);
    statusLabel = new JLabel("", SwingConstants.CENTER);
    statusLabel.setSize(350, 100);
    mainFrame.addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosing(final WindowEvent windowEvent) {
        System.exit(0);
      }
    });
    controlPanel = new JPanel();
    controlPanel.setLayout(new FlowLayout());
    mainFrame.add(headerLabel);
    mainFrame.add(controlPanel);
    mainFrame.add(statusLabel);
    mainFrame.setVisible(true);
  }

  private void showGroupLayoutDemo() {
    headerLabel.setText("Layout in action: GroupLayout");
    final JPanel panel = new JPanel();
    // panel.setBackground(Color.darkGray);
    panel.setSize(200, 200);
    final GroupLayout layout = new GroupLayout(panel);
    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);
    final JButton btn1 = new JButton("Button 1");
    final JButton btn2 = new JButton("Button 2");
    final JButton btn3 = new JButton("Button 3");
    layout.setHorizontalGroup(
        layout.createSequentialGroup().addComponent(btn1).addGroup(layout.createSequentialGroup(). //
            addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(btn2)
                .addComponent(btn3))));
    layout.setVerticalGroup(
        layout.createSequentialGroup().addComponent(btn1).addComponent(btn2).addComponent(btn3));
    panel.setLayout(layout);
    controlPanel.add(panel);
    mainFrame.setVisible(true);
  }
}
