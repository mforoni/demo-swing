package com.gitlab.mforoni.demo.swing.calculator;

import javax.swing.JFrame;

class Calculator {

  private Calculator() {} // Preventing instantiation

  /**
   * @param args
   */
  public static void main(final String[] args) {
    final CalculatorFrame calculatorFrame = new CalculatorFrame("My calculator");
    calculatorFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    calculatorFrame.setSize(300, 300);
    calculatorFrame.setVisible(true);
  }
}
