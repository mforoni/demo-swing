package com.gitlab.mforoni.demo.swing.table;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;

/*
 * vedi:
 * https://stackoverflow.com/questions/7519244/jar-bundler-using-osxadapter-causing-application-to-
 * lag-or-terminate/7519403#7519403
 */
/** @see https://stackoverflow.com/questions/7519244 */
@SuppressWarnings("serial")
public class TableAddTest extends JPanel implements Runnable {

  private static final int N_ROWS = 8;
  private static String[] header = {"ID", "String", "Number", "Boolean"};
  private final DefaultTableModel dtm = new DefaultTableModel(null, header) {

    @Override
    public Class<?> getColumnClass(final int col) {
      return getValueAt(0, col).getClass();
    }
  };
  private final JTable table = new JTable(dtm);
  private final JScrollPane scrollPane = new JScrollPane(table);
  private final JScrollBar vScroll = scrollPane.getVerticalScrollBar();
  private final JProgressBar jpb = new JProgressBar();
  private int row;
  private boolean isAutoScroll;

  public TableAddTest() {
    this.setLayout(new BorderLayout());
    jpb.setIndeterminate(true);
    this.add(jpb, BorderLayout.NORTH);
    final Dimension d = new Dimension(320, N_ROWS * table.getRowHeight());
    table.setPreferredScrollableViewportSize(d);
    for (int i = 0; i < N_ROWS; i++) {
      addRow();
    }
    scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    vScroll.addAdjustmentListener(new AdjustmentListener() {

      @Override
      public void adjustmentValueChanged(final AdjustmentEvent e) {
        isAutoScroll = !e.getValueIsAdjusting();
      }
    });
    this.add(scrollPane, BorderLayout.CENTER);
    final JPanel panel = new JPanel();
    panel.add(new JButton(new AbstractAction("Add Row") {

      @Override
      public void actionPerformed(final ActionEvent e) {
        addRow();
      }
    }));
    this.add(panel, BorderLayout.SOUTH);
  }

  private void addRow() {
    final char c = (char) ('A' + row++ % 26);
    dtm.addRow(new Object[] {Character.valueOf(c), String.valueOf(c) + String.valueOf(row),
        Integer.valueOf(row), Boolean.valueOf(row % 2 == 0)});
  }

  private void scrollToLast() {
    if (isAutoScroll) {
      final int last = table.getModel().getRowCount() - 1;
      final Rectangle r = table.getCellRect(last, 0, true);
      table.scrollRectToVisible(r);
    }
  }

  @Override
  public void run() {
    while (true) {
      EventQueue.invokeLater(new Runnable() {

        @Override
        public void run() {
          addRow();
        }
      });
      EventQueue.invokeLater(new Runnable() {

        @Override
        public void run() {
          scrollToLast();
        }
      });
      try {
        Thread.sleep(1000); // simulate latency
      } catch (final InterruptedException ex) {
        System.err.println(ex);
      }
    }
  }

  public static void main(final String[] args) {
    EventQueue.invokeLater(new Runnable() {

      @Override
      public void run() {
        final JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        final TableAddTest nlt = new TableAddTest();
        f.add(nlt);
        f.pack();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
        new Thread(nlt).start();
      }
    });
  }
}
