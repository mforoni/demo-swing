package com.gitlab.mforoni.demo.swing.calculator;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * @author ?
 */
class CalculatorFrame extends JFrame {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private final JTextField display;
  private final CalculatorPanel buttonsPanel;
  private double previousValue = 0.0;
  private char currentOperator = '?';
  private boolean resetDisplay = true;

  CalculatorFrame(final String titolo) {
    super(titolo);
    display = new JTextField();
    buttonsPanel = new CalculatorPanel(this);
    add(display, BorderLayout.NORTH);
    add(buttonsPanel, BorderLayout.CENTER);
    display.setEditable(false);
    display.setFont(new Font("SansSerif", Font.ITALIC, 22));
    display.setHorizontalAlignment(SwingConstants.RIGHT);
    display.setBackground(Color.WHITE);
  }

  protected void insertedDigit(final char digit) {
    if (resetDisplay) {
      display.setText("");
      resetDisplay = false;
    }
    display.setText(display.getText() + digit);
  }

  protected void insertedPoint(final char point) {
    if (resetDisplay) {
      display.setText("");
      resetDisplay = false;
    }
    display.setText(display.getText() + point);
  }

  protected void insertedOperator(final char operator) {
    if (operator == '=') {
      // if it has been pressed '=' then check if an operator was previously pressed:
      if (currentOperator != '?') {
        // if an operator was previously pressed then calculate the result between the previousValue
        // and the present displayed value:
        final double presentValue = Double.parseDouble(display.getText());
        final double result = calculatesResult(previousValue, presentValue, currentOperator);
        displayValue(result);
        currentOperator = '?';
        resetDisplay = true;
      }
    } else {
      // if it has not been pressed '=' check if an operator was previously pressed:
      if (currentOperator == '?') {
        // if an operator wasn't previously pressed then save the operator and the present displayed
        // value:
        previousValue = Double.parseDouble(display.getText());
        currentOperator = operator;
      } else {
        // if an operator has been previously inserted then calculates the result and save the value
        // as previous value:
        final double presentValue = Double.parseDouble(display.getText());
        previousValue = calculatesResult(previousValue, presentValue, currentOperator);
        display.setText(String.valueOf(previousValue));
        currentOperator = operator;
      }
      resetDisplay = true;
    }
  }

  private double calculatesResult(final double value1, final double value2, final char operator) {
    switch (operator) {
      case '+':
        return value1 + value2;
      case '-':
        return value1 - value2;
      case '*':
        return value1 * value2;
      case '/':
        return value1 / value2;
      default:
        return 0.0;
    }
  }

  private void displayValue(final double doubleValue) {
    if (doubleValue % 1 == 0) {
      final int intValue = (int) doubleValue;
      display.setText(String.valueOf(intValue));
    } else {
      display.setText(String.valueOf(doubleValue));
    }
  }
}
