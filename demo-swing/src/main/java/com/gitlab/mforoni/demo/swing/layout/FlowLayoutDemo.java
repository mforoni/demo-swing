package com.gitlab.mforoni.demo.swing.layout;

import java.awt.Container;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

/**
 * {@link FlowLayout} example.
 *
 * @author Foroni
 */
final class FlowLayoutDemo {

  private FlowLayoutDemo() {}

  public static void main(final String[] args) {
    final JFrame frame = new JFrame("FlowLayout.CENTER");
    frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    final Container pane = frame.getContentPane();
    pane.setLayout(new FlowLayout(FlowLayout.CENTER));
    pane.add(new JButton("Button 1"));
    pane.add(new JLabel("Label 2"));
    pane.add(new JButton("Button 3"));
    pane.add(new JLabel("Label Four (4)"));
    pane.add(new JButton("Button 5"));
    frame.pack();
    // frame.show(); // The method show() is deprecated, better use setVisible()
    frame.setVisible(true);
  }
}
