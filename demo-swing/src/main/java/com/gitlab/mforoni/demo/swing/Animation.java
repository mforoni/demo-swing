package com.gitlab.mforoni.demo.swing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Insets;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

/**
 * source:
 * http://www.java-tips.org/java-se-tips-100019/21-java-awt/2269-how-to-create-animation-paint-and-thread.html
 * 
 * @author Foroni Marco
 */
final class Animation extends JFrame { // Bad choiche prefer composition over inheritance

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private static final int DELAY = 100; // 1000;
  private Insets insets;
  private final Color colors[] =
      {Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.BLUE, Color.MAGENTA};

  @Override
  public void paint(final Graphics g) {
    super.paint(g);
    if (insets == null) {
      insets = getInsets();
    }
    // Calculate each time in case of resize
    final int x = insets.left;
    final int y = insets.top;
    final int width = getWidth() - insets.left - insets.right;
    final int height = getHeight() - insets.top - insets.bottom;
    int start = 0;
    final int steps = colors.length;
    final int stepSize = 360 / steps;
    synchronized (colors) {
      for (int i = 0; i < steps; i++) {
        g.setColor(colors[i]);
        g.fillArc(x, y, width, height, start, stepSize);
        start += stepSize;
      }
    }
  }

  public void go() {
    final TimerTask timerTask = new TimerTask() {

      @Override
      public void run() {
        final Color color = colors[0];
        synchronized (colors) {
          System.arraycopy(colors, 1, colors, 0, colors.length - 1);
          colors[colors.length - 1] = color;
        }
        repaint();
      }
    };
    final Timer timer = new Timer();
    timer.schedule(timerTask, 0, DELAY);
  }

  public static void main(final String[] args) {
    final Animation animate = new Animation();
    animate.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    animate.setSize(200, 200);
    // animate.show();
    animate.setVisible(true);
    animate.go();
  }
}
