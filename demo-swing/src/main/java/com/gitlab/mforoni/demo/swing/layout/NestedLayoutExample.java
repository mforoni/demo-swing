package com.gitlab.mforoni.demo.swing.layout;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

/*
 * See:
 * https://stackoverflow.com/questions/5621338/how-to-add-jtable-in-jpanel-with-null-layout/5630271
 */
/**
 * A short example of a nested layout that can change PLAF at runtime. The TitledBorder of each
 * JPanel shows the layouts explicitly set.
 * 
 * @author Andrew Thompson
 * @version 2011-04-12
 */
class NestedLayoutExample {

  public static void main(final String[] args) {
    final Runnable r = new Runnable() {

      @Override
      public void run() {
        final JFrame frame = new JFrame("Nested Layout Example");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        final JPanel gui = new JPanel(new BorderLayout(5, 5));
        gui.setBorder(new TitledBorder("BorderLayout(5,5)"));
        // JToolBar tb = new JToolBar();
        final JPanel plafComponents = new JPanel(new FlowLayout(FlowLayout.RIGHT, 3, 3));
        plafComponents.setBorder(new TitledBorder("FlowLayout(FlowLayout.RIGHT, 3,3)"));
        final UIManager.LookAndFeelInfo[] plafInfos = UIManager.getInstalledLookAndFeels();
        final String[] plafNames = new String[plafInfos.length];
        for (int ii = 0; ii < plafInfos.length; ii++) {
          plafNames[ii] = plafInfos[ii].getName();
        }
        // final JComboBox<String> plafChooser = new JComboBox<String>(plafNames);
        final JComboBox plafChooser = new JComboBox(plafNames);
        plafComponents.add(plafChooser);
        final JCheckBox pack = new JCheckBox("Pack on PLAF change", true);
        plafComponents.add(pack);
        plafChooser.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(final ActionEvent ae) {
            final int index = plafChooser.getSelectedIndex();
            try {
              UIManager.setLookAndFeel(plafInfos[index].getClassName());
              SwingUtilities.updateComponentTreeUI(frame);
              if (pack.isSelected()) {
                frame.pack();
                frame.setMinimumSize(frame.getSize());
              }
            } catch (final Exception e) {
              e.printStackTrace();
            }
          }
        });
        gui.add(plafComponents, BorderLayout.NORTH);
        final JPanel dynamicLabels = new JPanel(new BorderLayout(4, 4));
        dynamicLabels.setBorder(new TitledBorder("BorderLayout(4,4)"));
        gui.add(dynamicLabels, BorderLayout.WEST);
        final JPanel labels = new JPanel(new GridLayout(0, 2, 3, 3));
        labels.setBorder(new TitledBorder("GridLayout(0,2,3,3)"));
        final JButton addNew = new JButton("Add Another Label");
        dynamicLabels.add(addNew, BorderLayout.NORTH);
        addNew.addActionListener(new ActionListener() {

          private int labelCount = 0;

          @Override
          public void actionPerformed(final ActionEvent ae) {
            labels.add(new JLabel("Label " + ++labelCount));
            frame.validate();
          }
        });
        dynamicLabels.add(new JScrollPane(labels), BorderLayout.CENTER);
        final String[] header = {"Name", "Value"};
        final String[] a = new String[0];
        final String[] names = System.getProperties().stringPropertyNames().toArray(a);
        final String[][] data = new String[names.length][2];
        for (int ii = 0; ii < names.length; ii++) {
          data[ii][0] = names[ii];
          data[ii][1] = System.getProperty(names[ii]);
        }
        final DefaultTableModel model = new DefaultTableModel(data, header);
        final JTable table = new JTable(model);
        try {
          // 1.6+
          table.setAutoCreateRowSorter(true);
        } catch (final Exception continuewithNoSort) {
          System.out.println("Ignored exception:\n" + continuewithNoSort.getMessage());
        }
        final JScrollPane tableScroll = new JScrollPane(table);
        final Dimension tablePreferred = tableScroll.getPreferredSize();
        tableScroll
            .setPreferredSize(new Dimension(tablePreferred.width, tablePreferred.height / 3));
        final JPanel imagePanel = new JPanel(new GridBagLayout());
        imagePanel.setBorder(new TitledBorder("GridBagLayout()"));
        final BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
        final Graphics2D g = bi.createGraphics();
        final GradientPaint gp = new GradientPaint(20f, 20f, Color.red, 180f, 180f, Color.yellow);
        g.setPaint(gp);
        g.fillRect(0, 0, 200, 200);
        final ImageIcon ii = new ImageIcon(bi);
        final JLabel imageLabel = new JLabel(ii);
        imagePanel.add(imageLabel, null);
        final JSplitPane splitPane =
            new JSplitPane(JSplitPane.VERTICAL_SPLIT, tableScroll, new JScrollPane(imagePanel));
        gui.add(splitPane, BorderLayout.CENTER);
        frame.setContentPane(gui);
        frame.pack();
        frame.setLocationRelativeTo(null);
        try {
          // 1.6+
          frame.setLocationByPlatform(true);
          frame.setMinimumSize(frame.getSize());
        } catch (final Throwable ignoreAndContinue) {
        }
        frame.setVisible(true);
      }
    };
    SwingUtilities.invokeLater(r);
  }
}
