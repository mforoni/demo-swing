package com.gitlab.mforoni.demo.swing.calculator;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * @author ?
 */
class CalculatorPanel extends JPanel {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private final JButton[] buttons = { //
      new JButton("1"), new JButton("2"), new JButton("3"), new JButton("+"), //
      new JButton("4"), new JButton("5"), new JButton("6"), new JButton("-"), //
      new JButton("7"), new JButton("8"), new JButton("9"), new JButton("*"), //
      new JButton("0"), new JButton("."), new JButton("="), new JButton("/"), //
  };
  private final CalculatorFrame mainFrame;

  CalculatorPanel(final CalculatorFrame calcFrame) {
    mainFrame = calcFrame;
    setLayout(new GridLayout(4, 4));
    final ButtonsActionListener buttonsActionListener = new ButtonsActionListener();
    for (int i = 0; i < buttons.length; ++i) {
      buttons[i].setBackground(Color.CYAN);
      add(buttons[i]);
      buttons[i].addActionListener(buttonsActionListener);
    }
  }

  private class ButtonsActionListener implements ActionListener {

    @Override
    public void actionPerformed(final ActionEvent ev) {
      final JButton pressedButton = (JButton) ev.getSource();
      final char charButton = pressedButton.getText().charAt(0);
      // System.out.println(c);
      if ((charButton >= '0') && (charButton <= '9')) {
        mainFrame.insertedDigit(charButton);
      } else if (charButton == '.') {
        mainFrame.insertedPoint(charButton);
      } else {
        mainFrame.insertedOperator(charButton);
      }
    }
  }
}
