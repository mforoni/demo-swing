package com.gitlab.mforoni.demo.swing;

import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;

/**
 * https://stackoverflow.com/questions/7777640/best-practice-for-setting-jframe-locations/7778332#7778332
 * <p>
 * Si ricorda della posizione in cui l'utente aveva posizionato l'interfaccia anche dopo la
 * chiusura.
 * 
 * @author ?
 */
final class RestoreMe {

  /**
   * This will end up in the current directory A more sensible location is a sub-directory of
   * user.home. (left as an exercise for the reader)
   */
  public static final String FILENAME = "options.prop";

  private RestoreMe() {}

  public static void main(final String[] args) {
    final JFrame f = new JFrame("Good Location & Size");
    f.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    f.addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosing(final WindowEvent we) {
        try {
          storeOptions(f);
        } catch (final Exception e) {
          e.printStackTrace();
        }
        System.exit(0);
      }
    });
    final JTextArea jta = new JTextArea(20, 50);
    f.add(jta);
    f.pack();
    final File optionsFile = new File(FILENAME);
    if (optionsFile.exists()) {
      try {
        restoreOptions(f);
      } catch (final IOException ioe) {
        ioe.printStackTrace();
      }
    } else {
      f.setLocationByPlatform(true);
    }
    f.setVisible(true);
  }

  /** Restore location & size of UI */
  public static void restoreOptions(final Frame f) throws IOException {
    final File file = new File(FILENAME);
    final Properties p = new Properties();
    final BufferedReader br = new BufferedReader(new FileReader(file));
    try {
      p.load(br);
    } finally {
      br.close();
    }
    final int x = Integer.parseInt(p.getProperty("x"));
    final int y = Integer.parseInt(p.getProperty("y"));
    final int w = Integer.parseInt(p.getProperty("w"));
    final int h = Integer.parseInt(p.getProperty("h"));
    final Rectangle r = new Rectangle(x, y, w, h);
    f.setBounds(r);
  }

  /** Store location & size of UI */
  public static void storeOptions(final Frame f) throws IOException {
    final File file = new File(FILENAME);
    final Properties p = new Properties();
    // restore the frame from 'full screen' first!
    f.setExtendedState(Frame.NORMAL);
    final Rectangle r = f.getBounds();
    final int x = (int) r.getX();
    final int y = (int) r.getY();
    final int w = (int) r.getWidth();
    final int h = (int) r.getHeight();
    p.setProperty("x", "" + x);
    p.setProperty("y", "" + y);
    p.setProperty("w", "" + w);
    p.setProperty("h", "" + h);
    final BufferedWriter br = new BufferedWriter(new FileWriter(file));
    try {
      p.store(br, "Properties of the user frame");
    } finally {
      br.close();
    }
  }
}
