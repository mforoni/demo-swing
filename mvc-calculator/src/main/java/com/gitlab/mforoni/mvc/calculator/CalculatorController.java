package com.gitlab.mforoni.mvc.calculator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * http://www.newthinktank.com/2013/02/mvc-java-tutorial/
 * <p>
 * The Controller coordinates interactions between the View and Model.
 * 
 * @author newthinktank
 */
final class CalculatorController {

	private final CalculatorView theView;
	private final CalculatorModel theModel;

	public CalculatorController(final CalculatorView theView, final CalculatorModel theModel) {
		this.theView = theView;
		this.theModel = theModel;
		// Tell the View that when ever the calculate button
		// is clicked to execute the actionPerformed method
		// in the CalculateListener inner class
		this.theView.addCalculateListener(new CalculateListener());
	}

	class CalculateListener implements ActionListener {

		public void actionPerformed(final ActionEvent e) {
			int firstNumber, secondNumber = 0;
			// Surround interactions with the view with
			// a try block in case numbers weren't
			// properly entered
			try {
				firstNumber = theView.getFirstNumber();
				secondNumber = theView.getSecondNumber();
				theModel.addTwoNumbers(firstNumber, secondNumber);
				theView.setCalcSolution(theModel.getCalculationValue());
			} catch (final NumberFormatException ex) {
				System.out.println(ex);
				theView.displayErrorMessage("You Need to Enter 2 Integers");
			}
		}
	}
}