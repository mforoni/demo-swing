package com.gitlab.mforoni.mvc.calculator;

/**
 * 
 * @author newthinktank
 */
final class MVCCalculator {
  // http://www.newthinktank.com/2013/02/mvc-java-tutorial/

  public static void main(final String[] args) {
    final CalculatorView theView = new CalculatorView();
    final CalculatorModel theModel = new CalculatorModel();
    final CalculatorController theController = new CalculatorController(theView, theModel);
    theView.setVisible(true);
  }
}
