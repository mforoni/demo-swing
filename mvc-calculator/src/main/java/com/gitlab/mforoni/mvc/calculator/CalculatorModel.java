package com.gitlab.mforoni.mvc.calculator;

/**
 * http://www.newthinktank.com/2013/02/mvc-java-tutorial/
 * <p>
 * The Model performs all the calculations needed and that is it. It doesn't know the View exists
 * 
 * @author newthinktank
 */
final class CalculatorModel {

	// Holds the value of the sum of the numbers entered in the view
	private int calculationValue;

	public void addTwoNumbers(final int firstNumber, final int secondNumber) {
		calculationValue = firstNumber + secondNumber;
	}

	public int getCalculationValue() {
		return calculationValue;
	}
}