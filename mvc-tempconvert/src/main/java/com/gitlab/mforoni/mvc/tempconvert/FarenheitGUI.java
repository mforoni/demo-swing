package com.gitlab.mforoni.mvc.tempconvert;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

/**
 * http://csis.pace.edu/~bergin/mvc/mvcgui.html
 * 
 * @author Joseph Bergin
 */
final class FarenheitGUI extends TemperatureGUI {

	public FarenheitGUI(final TemperatureModel model, final int h, final int v) {
		super("Farenheit Temperature", model, h, v);
		setDisplay("" + model.getF());
		addUpListener(new UpListener());
		addDownListener(new DownListener());
		addDisplayListener(new DisplayListener());
	}

	// Called from the Model
	public void update(final Observable t, final Object o) {
		setDisplay("" + model().getF());
	}

	class UpListener implements ActionListener {

		public void actionPerformed(final ActionEvent e) {
			model().setF(model().getF() + 1.0);
		}
	}

	class DownListener implements ActionListener {

		public void actionPerformed(final ActionEvent e) {
			model().setF(model().getF() - 1.0);
		}
	}

	class DisplayListener implements ActionListener {

		public void actionPerformed(final ActionEvent e) {
			final double value = getDisplay();
			model().setF(value);
		}
	}
}