package com.gitlab.mforoni.mvc.tempconvert;

/**
 * http://csis.pace.edu/~bergin/mvc/mvcgui.html
 * 
 * @author Joseph Bergin
 */
final class TemperatureModel extends java.util.Observable {

	private double temperatureF = 32.0;

	public double getF() {
		return temperatureF;
	}

	public double getC() {
		return (temperatureF - 32.0) * 5.0 / 9.0;
	}

	public void setF(final double tempF) {
		temperatureF = tempF;
		setChanged();
		notifyObservers();
	}

	public void setC(final double tempC) {
		temperatureF = tempC * 9.0 / 5.0 + 32.0;
		setChanged();
		notifyObservers();
	}
}
