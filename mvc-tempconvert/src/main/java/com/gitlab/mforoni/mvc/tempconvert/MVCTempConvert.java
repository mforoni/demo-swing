package com.gitlab.mforoni.mvc.tempconvert;

/**
 * 
 * @author Joseph Bergin
 */
public class MVCTempConvert {
  // http://csis.pace.edu/~bergin/mvc/mvcgui.html

  public static void main(final String args[]) {
    final TemperatureModel temperature = new TemperatureModel();
    new FarenheitGUI(temperature, 100, 100);
    new CelsiusGUI(temperature, 100, 250);
  }
}
