package com.gitlab.mforoni.mvc.tempconvert;

import java.awt.Button;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * http://csis.pace.edu/~bergin/mvc/mvcgui.html
 * 
 * @author Joseph Bergin
 */
abstract class TemperatureGUI implements java.util.Observer {

	private final String label;
	private final TemperatureModel model;
	private final Frame temperatureFrame;
	private final TextField display = new TextField();
	private final Button upButton = new Button("Raise");
	private final Button downButton = new Button("Lower");

	TemperatureGUI(final String label, final TemperatureModel model, final int h, final int v) {
		this.label = label;
		this.model = model;
		temperatureFrame = new Frame(label);
		temperatureFrame.add("North", new Label(label));
		temperatureFrame.add("Center", display);
		final Panel buttons = new Panel();
		buttons.add(upButton);
		buttons.add(downButton);
		temperatureFrame.add("South", buttons);
		temperatureFrame.addWindowListener(new CloseListener());
		model.addObserver(this); // Connect the View to the Model
		temperatureFrame.setSize(200, 100);
		temperatureFrame.setLocation(h, v);
		temperatureFrame.setVisible(true);
	}

	public void setDisplay(final String s) {
		display.setText(s);
	}

	public double getDisplay() {
		double result = 0.0;
		try {
			result = Double.valueOf(display.getText()).doubleValue();
		} catch (final NumberFormatException e) {}
		return result;
	}

	public void addDisplayListener(final ActionListener a) {
		display.addActionListener(a);
	}

	public void addUpListener(final ActionListener a) {
		upButton.addActionListener(a);
	}

	public void addDownListener(final ActionListener a) {
		downButton.addActionListener(a);
	}

	protected TemperatureModel model() {
		return model;
	}

	public static class CloseListener extends WindowAdapter {

		@Override
		public void windowClosing(final WindowEvent e) {
			e.getWindow().setVisible(false);
			System.exit(0);
		}
	}
}