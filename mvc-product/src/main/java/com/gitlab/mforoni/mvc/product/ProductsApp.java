package com.gitlab.mforoni.mvc.product;

import javax.swing.JFrame;
import com.gitlab.mforoni.mvc.product.controller.ProductMainController;
import com.gitlab.mforoni.mvc.product.view.ProductMainPanel;

public class ProductsApp {


  public static void main(final String[] args) {
    final ProductMainPanel productMainPanel = new ProductMainPanel();
    ProductMainController productMainController = new ProductMainController(productMainPanel);
    final JFrame frame = new JFrame("Products App");
    frame.add(productMainPanel);
    frame.pack();
    frame.setVisible(true);;
  }
}
