package com.gitlab.mforoni.mvc.product;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConnectToDatabase {

  public static Connection createConnection() {
    String dbUrl = "jdbc:derby://localhost:1527/ProductManagerDb";
    Connection conn = null;
    try {
      conn = DriverManager.getConnection(dbUrl);
    } catch (SQLException ex) {
      Logger.getLogger(ConnectToDatabase.class.getName()).log(Level.SEVERE, null, ex);
    }
    return conn;
  }

}
