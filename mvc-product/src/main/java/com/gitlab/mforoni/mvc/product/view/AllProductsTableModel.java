package com.gitlab.mforoni.mvc.product.view;

import java.util.List;
import javax.swing.table.DefaultTableModel;
import com.gitlab.mforoni.mvc.product.model.Product;
import com.gitlab.mforoni.mvc.product.model.ProductService;


public class AllProductsTableModel extends DefaultTableModel {

  private final String[] columnNames = {"productId", "#", "Order number", "Description", "Width",
      "Depth", "Height", "Weight", "Price"};
  private final ProductService productService = new ProductService();
  private final List<Product> allProducts = productService.getAllProducts();

  public AllProductsTableModel() {
    addColumnNames();
    addData();
  }

  public void addColumnNames() {
    for (String columnName : columnNames) {
      super.addColumn(columnName);
    }
  }

  public void addData() {
    for (Product product : allProducts) {
      Object[] productData =
          {product.getProductId(), Boolean.FALSE, product.getProductOrderNumber(),
              product.getProductDescription(), product.getProductWidth(), product.getProductDepth(),
              product.getProductHeight(), product.getProductWeight(), product.getProductPrice()};
      super.addRow(productData);
    }
  }

  public void addProductDataToRow(Product product) {
    Object[] productData = {product.getProductId(), Boolean.FALSE, product.getProductOrderNumber(),
        product.getProductDescription(), product.getProductWidth(), product.getProductDepth(),
        product.getProductHeight(), product.getProductWeight(), product.getProductPrice()};
    super.addRow(productData);
  }

  public void updateProductDataInRow(Product product) {
    Object[] productData = {product.getProductOrderNumber(), product.getProductDescription(),
        product.getProductWidth(), product.getProductDepth(), product.getProductHeight(),
        product.getProductWeight(), product.getProductPrice()};
    for (int i = 0; i < getRowCount(); i++) {
      if (product.getProductId().equals((int) getValueAt(i, 0))) {
        for (int j = 0; j < productData.length; j++) {
          super.setValueAt(productData[j], i, j + 2);
        }
      }
    }
  }

  @Override
  public boolean isCellEditable(int row, int column) {
    return column == 1;
  }

  @Override
  public Class<?> getColumnClass(int columnIndex) {
    switch (columnIndex) {
      case 1:
        return Boolean.class;
      default:
        return String.class;
    }
  }

}
