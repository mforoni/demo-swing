package com.gitlab.mforoni.mvc.product.model;

import java.util.List;

public interface ProductDao {

  public void createProduct(Product product);

  public Product getProductById(int productId);

  public List<Product> getAllProducts();

  public void updateProduct(Product product);

  public void deleteProduct(int productId);

}
