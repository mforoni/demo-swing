package com.gitlab.mforoni.mvc.product.controller;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import com.gitlab.mforoni.mvc.product.model.Product;
import com.gitlab.mforoni.mvc.product.model.ProductService;
import com.gitlab.mforoni.mvc.product.view.AllProductsTableModel;
import com.gitlab.mforoni.mvc.product.view.ProductDetailPanel;
import com.gitlab.mforoni.mvc.product.view.ProductMainPanel;
import com.gitlab.mforoni.mvc.product.view.ProductNewPanel;

public class ProductDetailController {

  private final Product product;
  private final ProductDetailPanel productDetailPanel;
  private final ProductMainPanel productMainPanel;

  public ProductDetailController(Product product, ProductDetailPanel productDetailPanel,
      ProductMainPanel productMainPanel) {
    this.product = product;
    this.productDetailPanel = productDetailPanel;
    this.productMainPanel = productMainPanel;
    ProductFormButtonsPanelController productFormButtonsPanelController =
        new ProductFormButtonsPanelController();
  }

  private class ProductFormButtonsPanelController {

    public ProductFormButtonsPanelController() {
      productDetailPanel.addSaveProductBtnActionListener(new FormSaveButtonsActionListener(0));
      productDetailPanel
          .addSaveAndNewProductBtnActionListener(new FormSaveButtonsActionListener(1));
      productDetailPanel
          .addSaveAndCloseProductBtnActionListener(new FormSaveButtonsActionListener(2));
      productDetailPanel.addCloseProductBtnActionListener(new FormCloseButtonActionListener());
    }

    private class FormSaveButtonsActionListener implements ActionListener {

      private final int actionListenerNum;

      public FormSaveButtonsActionListener(int actionListenerNum) {
        this.actionListenerNum = actionListenerNum;
      }

      @Override
      public void actionPerformed(ActionEvent e) {
        product.setProductOrderNumber(productDetailPanel.getProductOrderNumberFieldText());
        product.setProductDescription(productDetailPanel.getProductDescriptionFieldText());
        product.setProductWidth(Double.parseDouble(productDetailPanel.getProductWidthFieldText()));
        product.setProductDepth(Double.parseDouble(productDetailPanel.getProductDepthFieldText()));
        product
            .setProductHeight(Double.parseDouble(productDetailPanel.getProductHeightFieldText()));
        product
            .setProductWeight(Double.parseDouble(productDetailPanel.getProductWeightFieldText()));
        product.setProductPrice(Double.parseDouble(productDetailPanel.getProductPriceFieldText()));
        ProductService productService = new ProductService();
        productService.updateProduct(product);
        JTable allProductsTable = productMainPanel.getAllProductsTable();
        AllProductsTableModel allProductsTableModel =
            (AllProductsTableModel) allProductsTable.getModel();
        allProductsTableModel.updateProductDataInRow(product);
        switch (actionListenerNum) {
          case 0:

            break;
          case 1:
            ProductNewPanel productNewPanel = new ProductNewPanel();
            productMainPanel.addPanelToMainPanel(productNewPanel, "productNewPanel");
            productMainPanel.changePanel("productNewPanel");
            ProductNewController productNewController =
                new ProductNewController(productNewPanel, productMainPanel);
            break;
          case 2:
            productMainPanel.changePanel("tablePanel");
            break;
        }
      }

    }

    private class FormCloseButtonActionListener implements ActionListener {

      @Override
      public void actionPerformed(ActionEvent e) {
        productMainPanel.changePanel("tablePanel");
      }

    }

  }

}
