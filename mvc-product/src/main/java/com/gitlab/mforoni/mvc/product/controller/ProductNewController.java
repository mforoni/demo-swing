package com.gitlab.mforoni.mvc.product.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import com.gitlab.mforoni.mvc.product.model.Product;
import com.gitlab.mforoni.mvc.product.model.ProductService;
import com.gitlab.mforoni.mvc.product.view.AllProductsTableModel;
import com.gitlab.mforoni.mvc.product.view.ProductDetailPanel;
import com.gitlab.mforoni.mvc.product.view.ProductMainPanel;
import com.gitlab.mforoni.mvc.product.view.ProductNewPanel;

public class ProductNewController {

  private final ProductNewPanel productNewPanel;
  private final ProductMainPanel productMainPanel;

  public ProductNewController(ProductNewPanel productNewPanel, ProductMainPanel productMainPanel) {
    this.productNewPanel = productNewPanel;
    this.productMainPanel = productMainPanel;
    ProductFormButtonsPanelController productFormButtonsPanelController =
        new ProductFormButtonsPanelController();
  }

  private class ProductFormButtonsPanelController {

    public ProductFormButtonsPanelController() {
      productNewPanel.addSaveProductBtnActionListener(new FormSaveButtonsActionListener(0));
      productNewPanel.addSaveAndNewProductBtnActionListener(new FormSaveButtonsActionListener(1));
      productNewPanel.addSaveAndCloseProductBtnActionListener(new FormSaveButtonsActionListener(2));
      productNewPanel.addCloseProductBtnActionListener(new FormCloseButtonActionListener());
    }

    private class FormSaveButtonsActionListener implements ActionListener {

      private final int actionListenerNum;

      public FormSaveButtonsActionListener(int actionListenerNum) {
        this.actionListenerNum = actionListenerNum;
      }

      @Override
      public void actionPerformed(ActionEvent e) {
        Product product = new Product();
        product.setProductOrderNumber(productNewPanel.getProductOrderNumberFieldText());
        product.setProductDescription(productNewPanel.getProductDescriptionFieldText());
        product.setProductWidth(Double.parseDouble(productNewPanel.getProductWidthFieldText()));
        product.setProductDepth(Double.parseDouble(productNewPanel.getProductDepthFieldText()));
        product.setProductHeight(Double.parseDouble(productNewPanel.getProductHeightFieldText()));
        product.setProductWeight(Double.parseDouble(productNewPanel.getProductWeightFieldText()));
        product.setProductPrice(Double.parseDouble(productNewPanel.getProductPriceFieldText()));
        ProductService productService = new ProductService();
        productService.createProduct(product);
        JTable allProductsTable = productMainPanel.getAllProductsTable();
        AllProductsTableModel allProductsTableModel =
            (AllProductsTableModel) allProductsTable.getModel();
        allProductsTableModel.addProductDataToRow(product);
        switch (actionListenerNum) {
          case 0:
            ProductDetailPanel productDetailPanel = new ProductDetailPanel(product);
            productMainPanel.addPanelToMainPanel(productDetailPanel, "productDetailPanel");
            productMainPanel.changePanel("productDetailPanel");
            ProductDetailController productDetailController =
                new ProductDetailController(product, productDetailPanel, productMainPanel);
            break;
          case 1:
            ProductNewPanel productNewPanel = new ProductNewPanel();
            productMainPanel.addPanelToMainPanel(productNewPanel, "productNewPanel");
            productMainPanel.changePanel("productNewPanel");
            ProductNewController productNewController =
                new ProductNewController(productNewPanel, productMainPanel);
            break;
          case 2:
            productMainPanel.changePanel("tablePanel");
            break;
        }
      }

    }

    private class FormCloseButtonActionListener implements ActionListener {

      @Override
      public void actionPerformed(ActionEvent e) {
        productMainPanel.changePanel("tablePanel");
      }

    }

  }

}
