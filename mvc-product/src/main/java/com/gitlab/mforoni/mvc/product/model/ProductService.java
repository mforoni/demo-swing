package com.gitlab.mforoni.mvc.product.model;

import java.util.List;

public class ProductService {

  private final ProductDao productDao = new ProductDaoMock();

  public void createProduct(Product product) {
    productDao.createProduct(product);
  }

  public Product getProductById(int productId) {
    return productDao.getProductById(productId);
  }

  public List<Product> getAllProducts() {
    return productDao.getAllProducts();
  }

  public void updateProduct(Product product) {
    productDao.updateProduct(product);
  }

  public void deleteProduct(int productId) {
    productDao.deleteProduct(productId);
  }

}
