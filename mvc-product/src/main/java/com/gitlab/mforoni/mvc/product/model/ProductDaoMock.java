package com.gitlab.mforoni.mvc.product.model;

import java.util.ArrayList;
import java.util.List;

public class ProductDaoMock implements ProductDao {

  private final List<Product> products = new ArrayList<>();

  public ProductDaoMock() {
    final Product p1 = new Product();
    p1.setProductId(1);
    p1.setProductDescription("desc");
    products.add(p1);
  }


  @Override
  public void createProduct(Product product) {
    // TODO Auto-generated method stub

  }

  @Override
  public Product getProductById(int productId) {
    return null;
  }

  @Override
  public List<Product> getAllProducts() {
    return products;
  }

  @Override
  public void updateProduct(Product product) {
    // TODO Auto-generated method stub

  }

  @Override
  public void deleteProduct(int productId) {
    // TODO Auto-generated method stub

  }

}
