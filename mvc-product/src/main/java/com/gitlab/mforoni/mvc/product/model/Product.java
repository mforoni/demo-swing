package com.gitlab.mforoni.mvc.product.model;


import java.util.Objects;

public class Product {

  private Integer productId;
  private String productOrderNumber;
  private String productDescription;
  private Double productWidth;
  private Double productDepth;
  private Double productHeight;
  private Double productWeight;
  private Double productPrice;

  public Integer getProductId() {
    return productId;
  }

  public void setProductId(Integer productId) {
    this.productId = productId;
  }

  public String getProductOrderNumber() {
    return productOrderNumber;
  }

  public void setProductOrderNumber(String productOrderNumber) {
    this.productOrderNumber = productOrderNumber;
  }

  public String getProductDescription() {
    return productDescription;
  }

  public void setProductDescription(String productDescription) {
    this.productDescription = productDescription;
  }

  public Double getProductWidth() {
    return productWidth;
  }

  public void setProductWidth(Double productWidth) {
    this.productWidth = productWidth;
  }

  public Double getProductDepth() {
    return productDepth;
  }

  public void setProductDepth(Double productDepth) {
    this.productDepth = productDepth;
  }

  public Double getProductHeight() {
    return productHeight;
  }

  public void setProductHeight(Double productHeight) {
    this.productHeight = productHeight;
  }

  public Double getProductWeight() {
    return productWeight;
  }

  public void setProductWeight(Double productWeight) {
    this.productWeight = productWeight;
  }

  public Double getProductPrice() {
    return productPrice;
  }

  public void setProductPrice(Double productPrice) {
    this.productPrice = productPrice;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 29 * hash + Objects.hashCode(this.productId);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Product other = (Product) obj;
    if (!Objects.equals(this.productId, other.productId)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "Product{" + "productId=" + productId + ", productOrderNumber=" + productOrderNumber
        + ", productDescription=" + productDescription + ", productWidth=" + productWidth
        + ", productDepth=" + productDepth + ", productHeight=" + productHeight + ", productWeight="
        + productWeight + ", productPrice=" + productPrice + '}';
  }

}
