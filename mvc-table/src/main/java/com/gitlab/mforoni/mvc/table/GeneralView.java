package com.gitlab.mforoni.mvc.table;

import java.awt.BorderLayout;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableModel;

final class GeneralView {

  private final JFrame frame;
  private final JTable table;
  private final JButton button;
  private final JComboBox<String> comboBox;

  public GeneralView(final TableModel tableModel) {
    frame = new JFrame();
    final JPanel panel = new JPanel();
    comboBox = new JComboBox<>();
    button = new JButton("filter");
    panel.add(comboBox);
    panel.add(button);
    table = new JTable();
    table.setModel(tableModel);
    frame.add(panel, BorderLayout.SOUTH);
    frame.add(new JScrollPane(table));
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  public void initializeView(final ComboBoxModel<String> comboBoxModel) {
    comboBox.setModel(comboBoxModel);
    frame.pack();
    frame.setVisible(true);
  }

  public JButton getFilterButton() {
    return button;
  }

  public JComboBox<String> getComboBox() {
    return comboBox;
  }

  public JTable getTable() {
    return table;
  }
}
