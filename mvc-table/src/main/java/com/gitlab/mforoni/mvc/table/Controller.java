package com.gitlab.mforoni.mvc.table;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

final class Controller implements ActionListener {

  private final TableRowSorter<TableModel> sorter;
  private final JComboBox<?> comboBox;

  public Controller(final JComboBox<?> comboBox, final JTable table) {
    final TableModel model = table.getModel();
    sorter = new TableRowSorter<>(model);
    table.setRowSorter(sorter);
    this.comboBox = comboBox;
  }

  @Override
  public void actionPerformed(final ActionEvent e) {
    final Object selectedItem = comboBox.getSelectedItem();
    if (selectedItem == null) {
      System.out.println("No selected item: nothing to filter");
    } else {
      final RowFilter<TableModel, Object> rf = RowFilter.regexFilter(selectedItem.toString(), 0);
      sorter.setRowFilter(rf);
    }
  }
}
