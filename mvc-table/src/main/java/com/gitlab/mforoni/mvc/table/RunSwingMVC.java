package com.gitlab.mforoni.mvc.table;

import java.awt.EventQueue;
import java.util.Arrays;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;

final class RunSwingMVC {
  // https://stackoverflow.com/questions/37190269/filter-jtable-using-a-jcombobox
  private RunSwingMVC() {}

  private static void showGUI() {
    final String[] header = new String[] {"Nome", "Nickname", "Mail", "Data Nascita", "Credito"};
    final Object[][] data =
        new Object[][] {new Object[] {"Marco", "For", "aaa@gmail.com", "07/09/1984", 33},
            new Object[] {"Gianni", "Red", "bbb@gmail.com", "03/12/1978", 100},
            new Object[] {"Andrea", "Andy", "ccc@gmail.com", "13/01/1988", 10}};
    final DefaultTableModel tableModel = new DefaultTableModel(data, header);
    final String[] names = new String[] {"", "Marco", "Gianni", "Andrea"};
    Arrays.sort(names);
    final ComboBoxModel<String> comboBoxModel = new DefaultComboBoxModel<>(names);
    final Model model = new Model(tableModel, comboBoxModel);
    final GeneralView view = new GeneralView(tableModel);
    final Controller controller = new Controller(view.getComboBox(), view.getTable());
    view.getFilterButton().addActionListener(controller);
    view.initializeView(model.getComboBoxModel());
  }

  public static void main(final String[] args) {
    EventQueue.invokeLater(new Runnable() {

      @Override
      public void run() {
        showGUI();
      }
    });
  }
}
