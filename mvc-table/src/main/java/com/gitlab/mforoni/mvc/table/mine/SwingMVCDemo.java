package com.gitlab.mforoni.mvc.table.mine;

import javax.swing.SwingUtilities;

/**
 * https://examples.javacodegeeks.com/core-java/java-swing-mvc-example/
 * <p>
 * We create SwingMVCDemo.java class which serve as main class to running our example.
 * 
 * @author javacodegeeks
 */
final class SwingMVCDemo {

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        try {
          createAndShowGUI();
        } catch (final Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  public static void createAndShowGUI() throws Exception {
    new View();
  }
}
