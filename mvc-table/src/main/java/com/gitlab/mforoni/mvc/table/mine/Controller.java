package com.gitlab.mforoni.mvc.table.mine;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 * https://examples.javacodegeeks.com/core-java/java-swing-mvc-example/
 * <p>
 * We create Controller.java class which implements the ActionListener interface, it will be invoked
 * as a result of a user’s action on a view (i.e., it will be invoked because of the filter button
 * click).
 * 
 * @author javacodegeeks
 */
final class Controller implements ActionListener {

  private JTextField searchTermTextField = new JTextField(26);
  private final DefaultTableModel model;

  public Controller(final JTextField searchTermTextField, final DefaultTableModel model) {
    super();
    this.searchTermTextField = searchTermTextField;
    this.model = model;
  }

  @Override
  public void actionPerformed(final ActionEvent e) {
    final String searchTerm = searchTermTextField.getText();
    if (searchTerm != null && !"".equals(searchTerm)) {
      final Object[][] newData = new Object[Constants.DATA.length][];
      int idx = 0;
      for (final Object[] o : Constants.DATA) {
        if ("*".equals(searchTerm.trim())) {
          newData[idx++] = o;
        } else {
          if (String.valueOf(o[0]).startsWith(searchTerm.toUpperCase().trim())) {
            newData[idx++] = o;
          }
        }
      }
      model.setDataVector(newData, Constants.TABLE_HEADER);
    } else {
      JOptionPane.showMessageDialog(null, "Search term is empty", "Error",
          JOptionPane.ERROR_MESSAGE);
    }
  }
}
