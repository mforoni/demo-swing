package com.gitlab.mforoni.mvc.table.mine;

import javax.swing.table.DefaultTableModel;

/**
 * https://examples.javacodegeeks.com/core-java/java-swing-mvc-example/
 * <p>
 * We create Model.java class which implements the TableModel interface (or, more likely, subclass
 * the AbstractTableModel class). The job of this TableModel implementation is to serve as the
 * interface between your data and the JTable as a view component. Also, we add a supplementary
 * Constants.java class contains constants used through our code.
 * 
 * @author javacodegeeks
 */
@SuppressWarnings("serial")
final class Model extends DefaultTableModel {
  // BAD choice: prefer composition over inheritance

  public Model() {
    super(Constants.DATA, Constants.TABLE_HEADER);
  }
}
