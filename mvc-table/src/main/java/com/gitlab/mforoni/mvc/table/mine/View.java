package com.gitlab.mforoni.mvc.table.mine;

import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

/**
 * https://examples.javacodegeeks.com/core-java/java-swing-mvc-example/
 * <p>
 * We create View.java class which contains our main UI components, a JTextField where the user can
 * enter a filter string, JButton to start the filter and JTable where the filter results are
 * displayed.
 * 
 * @author javacodegeeks
 */
final class View {

  public View() {
    // Create views swing UI components
    final JTextField searchTermTextField = new JTextField(26);
    final JButton filterButton = new JButton("Filter");
    final JTable table = new JTable();
    // Create table model
    final Model model = new Model();
    table.setModel(model);
    // Create controller
    final Controller controller = new Controller(searchTermTextField, model);
    filterButton.addActionListener(controller);
    // Set the view layout
    final JPanel ctrlPane = new JPanel();
    ctrlPane.add(searchTermTextField);
    ctrlPane.add(filterButton);
    final JScrollPane tableScrollPane = new JScrollPane(table);
    tableScrollPane.setPreferredSize(new Dimension(700, 182)); // FIXME better fixed dimension
                                                               // or dynamic?
    tableScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
        "Market Movers", TitledBorder.CENTER, TitledBorder.TOP));
    final JSplitPane splitPane =
        new JSplitPane(JSplitPane.VERTICAL_SPLIT, ctrlPane, tableScrollPane);
    splitPane.setDividerLocation(35);
    splitPane.setEnabled(false);
    // Display it all in a scrolling window and make the window appear
    final JFrame frame = new JFrame("Swing MVC Demo");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.add(splitPane);
    frame.pack();
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
  }
}
