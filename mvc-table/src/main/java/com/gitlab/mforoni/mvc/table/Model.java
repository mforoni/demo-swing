package com.gitlab.mforoni.mvc.table;

import javax.swing.ComboBoxModel;
import javax.swing.table.DefaultTableModel;

final class Model {

  private final DefaultTableModel tableModel;
  private final ComboBoxModel<String> comboBoxModel;

  public Model(final DefaultTableModel tableModel, final ComboBoxModel<String> comboBoxModel) {
    this.tableModel = tableModel;
    this.comboBoxModel = comboBoxModel;
  }

  public DefaultTableModel getTableModel() {
    return tableModel;
  }

  public ComboBoxModel<String> getComboBoxModel() {
    return comboBoxModel;
  }
}
