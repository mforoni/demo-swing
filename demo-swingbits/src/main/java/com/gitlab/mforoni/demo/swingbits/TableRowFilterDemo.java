package com.gitlab.mforoni.demo.swingbits;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import org.oxbow.swingbits.table.filter.TableRowFilterSupport;

final class TableRowFilterDemo {

  private TableRowFilterDemo() {
    throw new AssertionError();
  }

  public static void createAndShowGUI() {
    final JFrame frame = new JFrame();
    final Object[] columnNames = {"First Name", "Last Name", "Email"};
    final Object[][] data = { //
        new Object[] {"Mario", "Rossi", "mario.rossi@gmail.com"}, //
        new Object[] {"Alberto", "Neri", "alberto.neri@gmail.com"}};
    final DefaultTableModel model = new DefaultTableModel(data, columnNames);
    final JTable table = new JTable(model);
    TableRowFilterSupport.forTable(table).searchable(true).apply();
    frame.getContentPane().add(new JScrollPane(table));
    frame.pack();
    frame.setVisible(true);
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        createAndShowGUI();
      }
    });
  }
}
