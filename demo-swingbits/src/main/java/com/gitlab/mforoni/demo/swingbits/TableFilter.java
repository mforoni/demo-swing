package com.gitlab.mforoni.demo.swingbits;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.oxbow.swingbits.table.filter.IFilterChangeListener;
import org.oxbow.swingbits.table.filter.ITableFilter;
import org.oxbow.swingbits.table.filter.JTableFilter;
import org.oxbow.swingbits.table.filter.TableRowFilterSupport;

/**
 * Not working on Linux????
 * 
 * see https://github.com/eugener/oxbow/issues/11
 * 
 * @author Marco Foroni
 *
 */
public class TableFilter {
  final JTable table;
  JTableFilter tableFilter;

  public TableFilter() {

    final String[] cols = {"Checkbox", "Ints", "Strings", "Bools", "test"};
    Object[][] data = {{false, 1, "A", false, "hide"}, {false, 1, "B", false, "hide"},
        {false, 1, "C", false, "hide"}, {false, 3, "C", false, "hide"}};

    final DefaultTableModel model = newModel(cols, data);
    table = new JTable(model) {
      @Override
      public boolean isCellEditable(int row, int col) {
        return true;
      }

    };
    table.getTableHeader().setReorderingAllowed(false);

    tableFilter = new JTableFilter(table);
    tableFilter.addChangeListener(new IFilterChangeListener() {

      @Override
      public void filterChanged(ITableFilter<?> arg0) {
        System.out.println("Fitler Event");

      }

    });
    TableRowFilterSupport.forFilter(tableFilter).searchable(true).apply();

    JButton newDataButton = new JButton("New Data");
    newDataButton.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        for (int i = 0; i < table.getRowCount(); i++) {
          table.setValueAt(i + 5, i, 1);
        }
      }

    });


    JPanel buttonPanel = new JPanel();
    buttonPanel.add(newDataButton);

    JPanel panel = new JPanel(new BorderLayout());
    panel.add(new JScrollPane(table), BorderLayout.CENTER);
    panel.add(buttonPanel, BorderLayout.SOUTH);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.add(panel);
    frame.pack();
    frame.setVisible(true);
  }

  public void updateNewFilter() {
    tableFilter = new JTableFilter(table);
  }

  private DefaultTableModel newModel(final String[] cols, Object[][] data) {
    return new DefaultTableModel(data, cols) {
      @Override
      public Class<?> getColumnClass(int column) {
        Class returnValue;
        if ((column >= 0) && (column < getColumnCount()) && getRowCount() > 0
            && getValueAt(0, column) != null) {
          returnValue = getValueAt(0, column).getClass();
        } else {
          returnValue = Object.class;
        }
        return returnValue;
      }
    };
  }

  public static void main(String[] args) {
    new TableFilter();
  }
}
