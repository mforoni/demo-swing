package com.gitlab.mforoni.mvc.form.address;

/**
 * https://stackoverflow.com/questions/5217611/the-mvc-pattern-and-swing
 * 
 * @author ?
 */
final class AddressDemo {

  public static void main(final String[] args) {
    final AddressView view = new AddressView();
    final AddressModel model = new AddressModel();
    // make sure the view and model is fully initialized before letting the controller control them.
    final Controller controller = new AddressController(view, model);
    controller.showView();
  }
}
