package com.gitlab.mforoni.mvc.form.address;

import javax.swing.JFrame;

interface View {

  JFrame getFrame();
}
