package com.gitlab.mforoni.mvc.form.address;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

final class AddressView implements View {

  private final JTextField txtFirstName = new JTextField();
  private final JTextField txtLastName = new JTextField();
  private final JTextField txtAddress = new JTextField();
  private final JButton btnSave = new JButton("Save");
  private final JButton btnClear = new JButton("Clear");
  private final JFrame frame;

  public AddressView() {
    frame = new JFrame("Demo MVC address form");
    final JPanel centralPane = new JPanel();
    centralPane.setLayout(new GridLayout(3, 2));
    centralPane.add(new JLabel("First Name:"));
    centralPane.add(txtFirstName);
    centralPane.add(new JLabel("Last Name:"));
    centralPane.add(txtLastName);
    centralPane.add(new JLabel("Address Name:"));
    centralPane.add(txtAddress);
    final JPanel bottomPane = new JPanel();
    bottomPane.setLayout(new FlowLayout());
    bottomPane.add(btnSave);
    bottomPane.add(btnClear);
    frame.getContentPane().add(centralPane, BorderLayout.CENTER);
    frame.getContentPane().add(bottomPane, BorderLayout.SOUTH);
  }

  public JTextField getTxtFirstName() {
    return txtFirstName;
  }

  public JTextField getTxtLastName() {
    return txtLastName;
  }

  public JTextField getTxtAddress() {
    return txtAddress;
  }

  public JButton getBtnSave() {
    return btnSave;
  }

  public JButton getBtnClear() {
    return btnClear;
  }

  @Override
  public JFrame getFrame() {
    return frame;
  }
}
