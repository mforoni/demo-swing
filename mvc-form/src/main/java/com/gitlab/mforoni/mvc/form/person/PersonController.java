package com.gitlab.mforoni.mvc.form.person;

import java.awt.event.ActionEvent;
import java.util.Observable;
import javax.swing.AbstractAction;
import javax.swing.WindowConstants;

final class PersonController {

  private final PersonView view;
  private final PersonModel model;

  public PersonController(final PersonView view, final PersonModel model) {
    this.view = view;
    this.model = model;
    model.addObserver((Observable o, Object arg) -> {
      view.getTxtFirstName().setText(model.getFirstName());
      view.getTxtLastName().setText(model.getLastName());
      view.getTxtAddress().setText(model.getAddress());
    });
    initActionEvents();
    // TODO init view from model ???
  }

  private void initActionEvents() {
    view.getBtnClear().setAction(new AbstractAction("Clear") {

      @Override
      public void actionPerformed(final ActionEvent arg0) {
        model.setFirstName("");
        model.setLastName("");
        model.setAddress("");
      }
    });
    view.getBtnSave().setAction(new AbstractAction("Save") {

      @Override
      public void actionPerformed(final ActionEvent arg0) {
        final String firstName = view.getTxtFirstName().getText();
        final String lastName = view.getTxtLastName().getText();
        final String address = view.getTxtAddress().getText();
        // TODO validate etc.
        model.update(firstName, lastName, address);
        model.save();
      }
    });
    view.getFrame().setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
  }

  public void showView() {
    view.getFrame().pack();
    view.getFrame().setVisible(true);
  }
}
