package com.gitlab.mforoni.mvc.form.address;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.WindowConstants;

public class AddressController implements PropertyChangeListener, Controller {

  private final AddressView view;
  private final AddressModel model;

  public AddressController(final AddressView view, final AddressModel model) {
    this.view = view;
    this.model = model;
    // register the controller as the listener of the model
    this.model.addListener(this);
    setUpViewEvents();
  }

  // code for setting the actions to be performed when the user interacts to the view.
  @SuppressWarnings("serial")
  private void setUpViewEvents() {
    view.getBtnClear().setAction(new AbstractAction("Clear") {

      @Override
      public void actionPerformed(final ActionEvent arg0) {
        model.setFirstName("");
        model.setLastName("");
        model.setAddress("");
      }
    });
    view.getBtnSave().setAction(new AbstractAction("Save") {

      @Override
      public void actionPerformed(final ActionEvent arg0) {
        // TODO validate etc.
        model.setFirstName(view.getTxtFirstName().getText());
        model.setLastName(view.getTxtLastName().getText());
        model.setAddress(view.getTxtAddress().getText());
        model.save();
      }
    });
    view.getFrame().setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
  }

  @Override
  public void propertyChange(final PropertyChangeEvent evt) {
    final String propName = evt.getPropertyName();
    final Object newVal = evt.getNewValue();
    if ("address".equalsIgnoreCase(propName)) {
      view.getTxtAddress().setText((String) newVal);
    }
    // else if property (name) that fired the change event is first name property
    // else if property (name) that fired the change event is last name property
  }

  @Override
  public void showView() {
    view.getFrame().pack();
    view.getFrame().setVisible(true);
  }
}
