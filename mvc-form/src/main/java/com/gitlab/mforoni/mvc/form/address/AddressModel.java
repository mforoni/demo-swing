package com.gitlab.mforoni.mvc.form.address;

import java.beans.PropertyChangeListener;
import javax.swing.event.SwingPropertyChangeSupport;

final class AddressModel implements Model {

  private final SwingPropertyChangeSupport propChangeFirer;
  private String address;
  private String firstName;
  private String lastName;

  public AddressModel() {
    propChangeFirer = new SwingPropertyChangeSupport(this);
  }

  public void addListener(final PropertyChangeListener prop) {
    propChangeFirer.addPropertyChangeListener(prop);
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(final String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(final String address) {
    final String oldVal = this.address;
    this.address = address;
    // after executing this, the controller will be notified that the new address has been set. Its
    // then
    // the controller's task to decide what to do when the address in the model has changed.
    // Ideally, the
    // controller will update the view about this
    propChangeFirer.firePropertyChange("address", oldVal, address);
  }
  // some other setters for other properties & code for database interaction

  @Override
  public void save() {
    System.out.println(String.format("Saving data: first name=%s, last name=%s, address=%s",
        String.valueOf(firstName), String.valueOf(lastName), String.valueOf(address)));
  }
}
