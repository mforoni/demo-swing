package com.gitlab.mforoni.mvc.form.person;

import java.util.Observable;

final class PersonModel extends Observable {

  private String firstName;
  private String lastName;
  private String address;

  public PersonModel() {}

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(final String firstName) {
    this.firstName = firstName;
    setChanged();
    notifyObservers();
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(final String lastName) {
    this.lastName = lastName;
    setChanged();
    notifyObservers();
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(final String address) {
    final String oldVal = this.address;
    this.address = address;
    setChanged();
    notifyObservers();
  }

  public void update(final String firstName, final String lastName, final String address) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.address = address;
  }

  public void save() {
    System.out.println(String.format("Saving data: first name=%s, last name=%s, address=%s",
        String.valueOf(firstName), String.valueOf(lastName), String.valueOf(address)));
  }
}
