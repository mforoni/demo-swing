package com.gitlab.mforoni.mvc.form.person;


final class PersonDemo {

  private static void createAndShowGUI1() {
    final PersonModel model = new PersonModel();
    final PersonView view = new PersonView();
    final PersonController controller = new PersonController(view, model);
    controller.showView();
  }

  private static void createAndShowGUI2() {
    final PersonModel model = new PersonModel();
    final PersonView view = new PersonView();
    final PersonController controller = new PersonController(view, model);
    model.setFirstName("Mario");
    model.setLastName("Rossi");
    controller.showView();
  }

  public static void main(final String[] args) {
    createAndShowGUI2();
  }
}
