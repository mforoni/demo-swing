package com.gitlab.mforoni.mvc.counter;

import java.awt.Button;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Observable;

/**
 * http://www.austintek.com/mvc/<br>
 * View.java (C)<br>
 * Joseph Mack 2011, jmack (at) wm7d (dot) net, released under GPL v3 (or any later version)<br>
 * inspired by Joseph Bergin's MVC gui at http://csis.pace.edu/~bergin/mvc/mvcgui.html
 * <p>
 * View is the UI (it's a GUI). View shows the model's state, and allows the user to enter commands
 * which will change the model's state.
 * <p>
 * View is an Observer
 * <p>
 * <b>Warning</b>: This code works fine, but is not reusable. It's what you might write for a first
 * iteration. If you just want the reusable code and aren't interested in the explanation, hop to
 * ReusableView.<br>
 * For View to accept the controller reference (parameter to addController()), View must have the
 * declaration of Controller. The consequence of this requirement is that View is dependant on
 * Controller and hence not reusable. <br>
 * We didn't have the same reusability problem with Model, because Java had declared a base class
 * Observer. The problem of View not being reusable comes about because Java doesn't have a base
 * class Controller. Why isn't there a base class Controller? According to google, no-one has even
 * thought about it. I mused about the central role of the 30yr old MVC to OOP design patterns, and
 * wondered why someone hadn't written a controller base class. The base class had to be extendable
 * by classes that listen (presumably to UIs). Then I realised that Controller is an ActionListner.
 * 
 * @author Joseph Mack
 */
public class View implements java.util.Observer {

  // attributes as must be visible within class
  private final TextField myTextField;
  private final Button button;

  // private Model model; // Joe: Model is hardwired in,
  // needed only if view initialises model (which we aren't doing)
  View() {
    System.out.println("View()");
    // frame in constructor and not an attribute as doesn't need to be visible to whole class
    final Frame frame = new Frame("simple MVC");
    frame.add("North", new Label("counter"));
    myTextField = new TextField();
    frame.add("Center", myTextField);
    // panel in constructor and not an attribute as doesn't need to be visible to whole class
    final Panel panel = new Panel();
    button = new Button("PressMe");
    panel.add(button);
    frame.add("South", panel);
    frame.addWindowListener(new CloseListener());
    frame.setSize(200, 100);
    frame.setLocation(100, 100);
    frame.setVisible(true);
  } // View()

  // Called from the Model
  public void update(final Observable obs, final Object obj) {
    // who called us and what did they send?
    System.out
        .println("View : Observable is " + obs.getClass() + ", object passed is " + obj.getClass());
    // model Pull
    // ignore obj and ask model for value, to do this, the view has to know about the model
    // (which I decided I didn't want to do)
    // uncomment next line to do Model Pull
    // myTextField.setText("" + model.getValue());
    //
    // model Push
    // parse obj
    myTextField.setText("" + ((Integer) obj).intValue()); // obj is an Object, need to cast to
                                                          // an Integer
  } // update()

  // to initialise TextField
  public void setValue(final int v) {
    myTextField.setText("" + v);
  } // setValue()

  public void addController(final Controller controller) {
    System.out.println("View      : adding controller");
    button.addActionListener(controller); // need controller before adding it as a listener
  } // addController()

  // uncomment to allow controller to use view to initialise model
  // public void addModel(Model m){
  // System.out.println("View : adding model");
  // this.model = m;
  // } //addModel()
  //
  public static class CloseListener extends WindowAdapter {

    @Override
    public void windowClosing(final WindowEvent e) {
      e.getWindow().setVisible(false);
      System.exit(0);
    } // windowClosing()
  } // CloseListener
}
