package com.gitlab.mforoni.mvc.counter;

import java.awt.Button;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Observable;

/**
 * http://www.austintek.com/mvc/<br>
 * View.java (C)<br>
 * Joseph Mack 2011, jmack (at) wm7d (dot) net, released under GPL v3 (or any later version)<br>
 * inspired by Joseph Bergin's MVC gui at http://csis.pace.edu/~bergin/mvc/mvcgui.html
 * <p>
 * The View pt2<br>
 * ReusableView is reusable by controllers which are ActionListeners, but not controllers which are
 * other types of Listeners. Presumably View can be extended for all listeners. Perhaps no-one needs
 * a base class for Controller.<br>
 * The view is an Observer.<br>
 * The view is a GUI which has:
 * <ul>
 * <li>a TextField to display the status of the model (the value of the counter)</li>
 * <li>a Button to communicate with the controller (the controller is a button Listener).</li>
 * </ul>
 * The trivial functionality of View is a constructor which generates the GUI and a method to
 * initialize the TextField. The interesting functionality of View communicates with the controller
 * and the model
 * <ul>
 * <li>a method addController(ActionListener controller), which attaches the controller as a
 * listener to the button (called by the glue class RunMVC)
 * <li>the magic part, update(), which receives the status message from model.</li>
 * </ul>
 * How does myView.update() get updated? (It all happens inside the instance Observable:myModel)
 * <ul>
 * <li>model changes state when the method Model:incrementValue() is executed (by the controller).
 * After first changing the model's state, Observable:setChanged() changes the flag
 * Observable:changed to true.</li>
 * <li>next Model:notifyObservers(counter) is run. notifyObservers(counter) is a method of
 * Observable. notifyObservers() checks that changed is true, sets it to false, looks up the vector
 * of observers, in our case finding myView, and then runs myView.update(Observable myModel.Object
 * (Integer)counter).</li>
 * <li>myView now has the reference to the observable myModel and a reference to its (new) status.
 * Subsequent commands in update() present the model's (new) status to the user.</li>
 * </ul>
 * 
 * @author Joseph Mack
 */
public class ReusableView implements java.util.Observer {

  // attributes as must be visible within class
  private final TextField myTextField;
  private final Button button;

  // private Model model; //Joe: Model is hardwired in,
  // needed only if view initialises model (which we aren't doing)
  ReusableView() {
    System.out.println("View()");
    // frame in constructor and not an attribute as doesn't need to be visible to whole class
    final Frame frame = new Frame("simple MVC");
    frame.add("North", new Label("counter"));
    myTextField = new TextField();
    frame.add("Center", myTextField);
    // panel in constructor and not an attribute as doesn't need to be visible to whole class
    final Panel panel = new Panel();
    button = new Button("PressMe");
    panel.add(button);
    frame.add("South", panel);
    frame.addWindowListener(new CloseListener());
    frame.setSize(200, 100);
    frame.setLocation(100, 100);
    frame.setVisible(true);
  } // View()

  // Called from the Model
  public void update(final Observable obs, final Object obj) {
    // who called us and what did they send?
    System.out
        .println("View : Observable is " + obs.getClass() + ", object passed is " + obj.getClass());
    // model Pull
    // ignore obj and ask model for value,
    // to do this, the view has to know about the model (which I decided I didn't want to do)
    // uncomment next line to do Model Pull
    // myTextField.setText("" + model.getValue());
    //
    // model Push
    // parse obj
    myTextField.setText("" + ((Integer) obj).intValue()); // obj is an Object, need to cast to
                                                          // an Integer
  } // update()

  // to initialise TextField
  public void setValue(final int v) {
    myTextField.setText("" + v);
  } // setValue()

  public void addController(final ActionListener controller) {
    System.out.println("View      : adding controller");
    button.addActionListener(controller); // need instance of controller before can add it as a
                                          // listener
  } // addController()

  // uncomment to allow controller to use view to initialise model
  // public void addModel(Model m){
  // System.out.println("View : adding model");
  // this.model = m;
  // } //addModel()
  //
  public static class CloseListener extends WindowAdapter {

    @Override
    public void windowClosing(final WindowEvent e) {
      e.getWindow().setVisible(false);
      System.exit(0);
    } // windowClosing()
  } // CloseListener
}
