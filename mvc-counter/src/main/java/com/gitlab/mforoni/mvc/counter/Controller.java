package com.gitlab.mforoni.mvc.counter;

/**
 * http://www.austintek.com/mvc/<br>
 * Controller.java<br>
 * (C) Joseph Mack 2011, jmack (at) wm7d (dot) net, released under GPL v3 (or any later version)<br>
 * inspired by Joseph Bergin's MVC gui at http://csis.pace.edu/~bergin/mvc/mvcgui.html
 * <p>
 * Controller is a Listener. It
 * <ul>
 * <li>has a method actionPerformed(), which listens to View's button. When the method receives a
 * button press, it changes the state of Model by running myModel.incrementValue().</li>
 * <li>has code which is specific to Model and View. Controller is not reusable.</li>
 * </ul>
 * Controller has the following routine functionality
 * <ul>
 * <li>a constructor which sends a notice to the console.</li>
 * <li>a method to initialise the Model.</li>
 * <li>methods to get references to myModel and myView (these methods are run by the glue class
 * RunMVC).</li>
 * </ul>
 * 
 * @author Joseph Mack
 */
public class Controller implements java.awt.event.ActionListener {

  // Joe: Controller has Model and View hardwired in
  Model model;
  View view;

  Controller() {
    System.out.println("Controller()");
  } // Controller()

  // invoked when a button is pressed
  public void actionPerformed(final java.awt.event.ActionEvent e) {
    // uncomment to see what action happened at view
    System.out.println("Controller: The " + e.getActionCommand() + " button is clicked at "
        + new java.util.Date(e.getWhen()) + " with e.paramString " + e.paramString());
    System.out.println("Controller: acting on Model");
    model.incrementValue();
  } // actionPerformed()

  // Joe I should be able to add any model/view with the correct API
  // but here I can only add Model/View
  public void addModel(final Model m) {
    System.out.println("Controller: adding model");
    this.model = m;
  } // addModel()

  public void addView(final View v) {
    System.out.println("Controller: adding view");
    this.view = v;
  } // addView()

  public void initModel(final int x) {
    model.setValue(x);
  } // initModel()
}
